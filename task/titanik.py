import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_title(name):
    for title in ["Mr.", "Mrs.", "Miss."]:
        if title in name:
            return title
    return None


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    res = []

    df["Title"] = df["Name"].apply(get_title)
    df = df[df["Title"].isin(titles)]
    medians = df.groupby("Title")["Age"].median().round()
    missing = df[df["Age"].isnull()]["Title"].value_counts()

    res.append(("Mr.", missing.get("Mr.", 0), int(medians.get("Mr."))))
    res.append(("Mrs.", missing.get("Mrs.", 0), int(medians.get("Mrs."))))
    res.append(("Miss.", missing.get("Miss.", 0), int(medians.get("Miss."))))

    return res
